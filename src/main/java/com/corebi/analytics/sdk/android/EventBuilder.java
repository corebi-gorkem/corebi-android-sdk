package com.corebi.analytics.sdk.android;

import java.util.Hashtable;

public class EventBuilder extends PropBuilder{

    private String type;

    private Long time;

    public EventBuilder() {
        super(new Hashtable<String, Object>());
    }

    public Event build() {
        return new Event(time, type, properties);
    }

    public EventBuilder setType(String type) {
        this.type = type;
        return this;
    }

    public EventBuilder setTime(long time) {
        this.time = time;
        return this;
    }
}

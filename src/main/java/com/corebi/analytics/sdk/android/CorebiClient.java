package com.corebi.analytics.sdk.android;

import android.util.Log;
import okhttp3.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by gorkem on 27.02.2018.
 */
public class CorebiClient {

    private final String propPrefix;

    private final String endPoint;

    private final String apiKey;

    private final int eventTreshold;

    private final long timeTreshold;

    private final Timer tresholdTimer;

    private final OkHttpClient http;

    private final GlobalProperties globalProperties;

    private final boolean isLogging;

    private int currentEventCount = 0;

    private List<Event> eventQueue;

    private ExecutorService executor;


    public CorebiClient(CorebiConfig corebiConfig) {
        propPrefix = corebiConfig.getPropPrefix();
        endPoint = corebiConfig.getEndPoint();
        apiKey = corebiConfig.getApiKey();
        eventTreshold = corebiConfig.getEventTreshold();
        timeTreshold = corebiConfig.getTimeTreshold();
        tresholdTimer = new Timer("Treshold Timer");
        setTimer();
        http = new OkHttpClient();
        eventQueue = new ArrayList<>();
        globalProperties = new GlobalProperties();
        executor = Executors.newSingleThreadExecutor();
        isLogging = corebiConfig.getLogging();
    }

    public GlobalProperties globalProperties() {
        return globalProperties;
    }

    private void resetTimer() {
        tresholdTimer.cancel();
        tresholdTimer.purge();
        setTimer();
    }

    private void setTimer() {
        tresholdTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                flush();
            }
        }, timeTreshold, timeTreshold);
    }

    public void send(Event event) {
        Event $event = event.withApiKey(apiKey);
        if (event.getTime() == null) {
            $event = $event.withTime(System.currentTimeMillis());
        }
        final Map<String, Object> $properties = new HashMap<>();
        for (Map.Entry<String, Object> property : globalProperties.properties.entrySet()) {
            $properties.put(propPrefix + property.getKey(), property.getValue());
        }
        for (Map.Entry<String, Object> property : event.getProperties().entrySet()) {
            $properties.put(propPrefix + property.getKey(), property.getValue());
        }
        $properties.put("tz", TimeZone.getDefault().getRawOffset());
        $event = $event.withProperties($properties);
        synchronized (this) {
            eventQueue.add($event);
            currentEventCount++;
            if (currentEventCount >= eventTreshold) {
                flush();
                currentEventCount = 0;
                //TODO: reset timer
            }
        }
        if (isLogging) Log.d("COREBI", "Send event : " + event.getType());
    }

    private void flush() {
        if (eventQueue.size() == 0) {
            return;
        }
        executor.submit(new Runnable() {
            @Override
            public void run() {
                List<Event> flushList = eventQueue;
                eventQueue = new ArrayList<>();
                JSONArray jsonArray = new JSONArray();
                for (Event event : flushList) {
                    try {
                        jsonArray.put(toJson(event));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                String eventsJson = jsonArray.toString();
                RequestBody body = RequestBody.create(MediaType.parse("application/json"), eventsJson);
                Request request = new Request.Builder()
                        .url(endPoint + "collector/log/bulk")
                        .post(body)
                        .build();
                try (Response response = http.newCall(request).execute()) {
                    if (isLogging) Log.d("COREBI", response.body().string());
                } catch (IOException e) {
                    if (isLogging) Log.e("COREBI", e.getMessage(), e);
                }
            }
        });
    }

    private JSONObject toJson(Event event) throws JSONException {
        JSONObject eventJson = new JSONObject();
        eventJson.put("type", event.getType());
        eventJson.put("apiKey", event.getApiKey());
        eventJson.put("properties", new JSONObject(event.getProperties()));
        return eventJson;
    }
}

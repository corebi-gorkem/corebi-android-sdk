package com.corebi.analytics.sdk.android;

import java.util.Map;

/**
 * Created by gorkem on 27.02.2018.
 */
public class Corebi {

    private static CorebiClient corebiClient;

    public static void initialize(CorebiConfig corebiConfig){
        corebiClient = new CorebiClient(corebiConfig);
    }

    public static void send(Event event) {
        corebiClient.send(event);
    }

    public static GlobalProperties globalProperties() {
        return corebiClient.globalProperties();
    }
}

package com.corebi.analytics.sdk.android;

import java.util.Collections;
import java.util.Map;

/**
 * Created by gorkem on 27.02.2018.
 */
public class Event {

    private final String type;

    private final String apiKey;

    private final Long time;

    private final Map<String, Object> properties;

    public Event(String type, String apiKey, Long time, Map<String, Object> properties) {
        this.type = type;
        this.apiKey = apiKey;
        this.time = time;
        this.properties = properties;
    }

    public Event(Long time, String type, Map<String, Object> properties) {
        this.type = type;
        this.properties = properties;
        apiKey = null;
        this.time = time;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getType() {
        return type;
    }

    public Long getTime() {
        return time;
    }

    public Map<String, Object> getProperties() {
        return Collections.unmodifiableMap(properties);
    }

    public Event withProperties(Map<String, Object> properties) {
        return new Event(type, apiKey, time, properties);
    }

    public Event withTime(long time) {
        return new Event(type, apiKey, time, properties);
    }

    public Event withApiKey(String apiKey) {
        return new Event(type, apiKey, time, properties);
    }

}

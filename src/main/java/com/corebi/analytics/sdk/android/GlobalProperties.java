package com.corebi.analytics.sdk.android;

import java.util.Hashtable;

public class GlobalProperties extends PropBuilder{
    protected GlobalProperties() {
        super(new Hashtable<String, Object>());
    }

    public void remove(String key) {
        properties.remove(key);
    }

    @Override
    public Event build() {
        throw new UnsupportedOperationException("Global properties doesn't build events");
    }
}

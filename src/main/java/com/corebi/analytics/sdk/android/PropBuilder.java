package com.corebi.analytics.sdk.android;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

public abstract class PropBuilder {

    protected final Map<String, Object> properties;

    protected PropBuilder(Map<String, Object> properties) {
        this.properties = properties;
    }

    public PropBuilder addProp(String key, Boolean value) {
        return addProp(key, (Object) value);
    }

    public PropBuilder addProp(String key, String value) {
        return addProp(key, (Object) value);
    }

    public PropBuilder addProp(String key, Integer value) {
        return addProp(key, (Object) value);
    }

    public PropBuilder addProp(String key, Long value) {
        return addProp(key, (Object) value);
    }

    public PropBuilder addProp(String key, Double value) {
        return addProp(key, (Object) value);
    }

    public PropBuilder addProp(String key, Date value) {
        return addProp(key, (Object) value);
    }

    public PropBuilder addProp(String key, Timestamp value) {
        return addProp(key, (Object) value);
    }

    public PropBuilder addProp(String key, Boolean[] value) {
        return addProp(key, (Object) value);
    }

    public PropBuilder addProp(String key, String[] value) {
        return addProp(key, (Object) value);
    }

    public PropBuilder addProp(String key, Integer[] value) {
        return addProp(key, (Object) value);
    }

    public PropBuilder addProp(String key, Long[] value) {
        return addProp(key, (Object) value);
    }

    public PropBuilder addProp(String key, Double[] value) {
        return addProp(key, (Object) value);
    }

    public PropBuilder addProp(String key, Date[] value) {
        return addProp(key, (Object) value);
    }

    public PropBuilder addProp(String key, Timestamp[] value) {
        return addProp(key, (Object) value);
    }

    public abstract Event build();

    private PropBuilder addProp(String key, Object value) {
        properties.put(key.toLowerCase(), value);
        return this;
    }
}
package com.corebi.analytics.sdk.android;

import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

/**
 * Created by gorkem on 27.02.2018.
 */
public class CorebiConfig {

    private Boolean logging = true;

    private String endPoint = "https://log.corebi.com/rest-api/v1/";

    private String apiKey;

    private int eventTreshold = 10;

    private long timeTreshold = TimeUnit.MINUTES.toMillis(1);

    private String propPrefix = "m_";

    public Boolean getLogging() {
        return logging;
    }

    public CorebiConfig setLogging(Boolean logging) {
        this.logging = logging;
        return this;
    }

    public String getPropPrefix() {
        return propPrefix;
    }

    public void setPropPrefix(String propPrefix) {
        this.propPrefix = propPrefix;
    }

    public String getEndPoint() {
        return endPoint;
    }

    public CorebiConfig setEndPoint(String endPoint) {
        this.endPoint = endPoint;
        return this;
    }

    public String getApiKey() {
        return apiKey;
    }

    public CorebiConfig setApiKey(String apiKey) {
        this.apiKey = apiKey;
        return this;
    }

    public int getEventTreshold() {
        return eventTreshold;
    }

    public CorebiConfig setEventTreshold(int eventTreshold) {
        this.eventTreshold = eventTreshold;
        return this;
    }

    public long getTimeTreshold() {
        return timeTreshold;
    }

    public CorebiConfig setTimeTreshold(long timeTreshold) {
        this.timeTreshold = timeTreshold;
        return this;
    }
}

package com.corebi.analytics.sdk.android.example;

import com.corebi.analytics.sdk.android.*;

import java.util.concurrent.TimeUnit;

public class SendEmosEvents {

    private static final String PLATFORM = "Desktop";

    private static final String USER_AGENT = "Example Application Agent";

    private static final String RESOLUTION = "1920x1080";

    public static void main(String[] args) {
        CorebiConfig corebiConfig = new CorebiConfig()
                .setLogging(false)
                .setApiKey("5dc7705c-6a92-4fa9-a0be-c8f8d458ee7c")
                .setEventTreshold(1)
                .setTimeTreshold(TimeUnit.MINUTES.toMillis(5));
        Corebi.initialize(corebiConfig);

        Corebi.globalProperties().addProp("platform", PLATFORM);
        Corebi.globalProperties().addProp("user_agent", USER_AGENT);
        Corebi.globalProperties().addProp("resolution", RESOLUTION);
        sendLoginEvent("USR1",
                "RICHARD",
                "FEYNMANN",
                "Perakende",
                "richard@gmail.com",
                "1997-6-24",
                "male");
        sendSignupEvent("USR2",
                "MARY",
                "CURIE",
                "Perakende",
                "mary@gmail.com",
                "1997-6-14",
                "female");
        sendTransactionEvent(
                new String[]{"p001", "p002"},
                new Double[]{2.9, 1.3},
                new Integer[]{2, 1},
                7.2,
                new String[]{"Cat1", "Cat3"},
                "ORDER101");
    }

    public static void sendTransactionEvent(String[] productCodes, Double[] productPrices, Integer[] productQuantities,
                                            Double orderPrice, String[] categoryNames, String orderId) {

        Event event = new EventBuilder().setType("transaction")
                .addProp("product_codes", productCodes)
                .addProp("product_prices", productPrices)
                .addProp("product_quantities", productQuantities)
                .addProp("order_price", orderPrice)
                .addProp("category_names", categoryNames)
                .addProp("order_id", orderId)
                .build();
        Corebi.send(event);
    }

    public static void sendProductViewEvent(String productCode, String productName, String brand, Double price,
                                       String categoryName, String productStock) {
        Event event = new EventBuilder().setType("product_view")
                .addProp("product_code", productCode)
                .addProp("product_name", productName)
                .addProp("brand", brand)
                .addProp("price", price)
                .addProp("category_name", categoryName)
                .addProp("product_stock", productStock)
                .build();
        Corebi.send(event);
    }

    public static void sendProductListEvent(String mainCategory, String categoryNames, Boolean filterInStock,
                                            String sort) {
        Event event = new EventBuilder().setType("product_list")
                .addProp("main_category", mainCategory)
                .addProp("category_names", categoryNames)
                .addProp("filter_in_stock", filterInStock)
                .addProp("sort", sort)
                .build();
        Corebi.send(event);
    }

    public static void sendSignupEvent(String userId, String name, String surname, String ftpName, String email,
                                       String birthday, String gender) {
        Corebi.globalProperties()
                .addProp("user_id", userId)
                .addProp("name", name)
                .addProp("surname", surname)
                .addProp("ftp_name", ftpName)
                .addProp("email", email)
                .addProp("birthday", birthday)
                .addProp("gender", gender);
        Event event = new EventBuilder().setType("signup").build();
        Corebi.send(event);
    }

    public static void sendLoginEvent(String userId, String name, String surname, String ftpName, String email,
                                       String birthday, String gender) {
        Corebi.globalProperties()
                .addProp("user_id", userId)
                .addProp("name", name)
                .addProp("surname", surname)
                .addProp("ftp_name", ftpName)
                .addProp("email", email)
                .addProp("birthday", birthday)
                .addProp("gender", gender);
        Event event = new EventBuilder().setType("login").build();
        Corebi.send(event);
    }

    public static void sendBasketViewEvent(Integer basketId, Integer totalProductCount,  String[] productCodes,
                                           String[] productBrands, Double[] productUnitPrices,
                                           Integer[] productQuantities) {

        Event event = new EventBuilder().setType("basket_view")
                .addProp("basket_id", basketId)
                .addProp("product_codes", productCodes)
                .addProp("product_brands", productBrands)
                .addProp("product_unit_prices", productUnitPrices)
                .addProp("product_quantities", productQuantities)
                .addProp("total_product_count", totalProductCount)
                .build();
        Corebi.send(event);
    }

    public static void sendSearchEvent(String searchTerm) {
        Event event = new EventBuilder().setType("search")
                .addProp("searc_term", searchTerm)
                .build();
        Corebi.send(event);
    }
}

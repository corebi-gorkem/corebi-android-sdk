# COREBI Android SDK
#### Installation
##### Gradle
```gradle
repositories {
    ...
    maven {
        url "https://repo.corebi.com/maven/"
    }
    ...
}
```

##### Maven
````xml
<dependency>
    <groupId>com.corebi.analytics</groupId>
    <artifactId>android-sdk</artifactId>
    <version>1.0.2</version>
</dependency>
````

#### Initialize
````java
import com.corebi.analytics.sdk.android.Corebi;
import com.corebi.analytics.sdk.android.CorebiConfig;

...

CorebiConfig corebiConfig = new CorebiConfig()
    .setApiKey("5dc7705c-6a92-4fa9-a0be-c8f8d458ee7c")
    .setEventTreshold(10)
    .setTimeTreshold(TimeUnit.MINUTES.toMillis(5));
Corebi.initialize(corebiConfig);
````

#### Global Properties
````java
import com.corebi.analytics.sdk.android.Corebi;
...
Corebi.globalProperties().addProp("uuid", "TEST");
````

#### Build and Send Event
````java
import com.corebi.analytics.sdk.android.Corebi;
import com.corebi.analytics.sdk.android.EventBuilder;
...
EventBuilder eventBuilder = new EventBuilder()
    .setType("test_click2_event");
eventBuilder.addProp("is_clicked2", true);
Corebi.send(eventBuilder.build());
````
